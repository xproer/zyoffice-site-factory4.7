﻿CKEDITOR.plugins.add('zyofficepdf',
{
	init: function(editor)
	{
		editor.addCommand('zyofficepdf',
		{
			exec: function(editor)
            {
                zyOffice.getInstance().SetEditor(editor);
				zyOffice.getInstance().api.openPdf();
			}
		});
		editor.ui.addButton('zyofficepdf',
		{
            label: '导入PDF文档',
			command: 'zyofficepdf',
			icon: this.path + 'images/pdf.png'
		});
	}
});
